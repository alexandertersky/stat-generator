<?php

namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'crm');
set('branch', 'dotenv');

set('default_timeout', 3600);

set('release_name', function () {
    return date('Ymd-H-i-s');
});

// Project repository
set('repository', 'git@bitbucket.org:seryak/slim-skeleton.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', ['configs', 'vendor', 'public/files']);

// Writable dirs by web server 
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('5.101.126.240')
    ->stage('dwtolk')
    ->user('deployer')
    ->set('deploy_path', '/home/developer/web/skeleton.dwtolk.ru/public_html');


// Tasks
task('servant_migrate', function () {
    run('/usr/bin/php ' . get('deploy_path') . '/current/console/servant/servant migrate');
});

task('app_version_iterate', function (){
    run('/usr/bin/php ' . get('deploy_path') . '/current/console/servant/servant app_version_iterate');
});

task('copy_migrations', function (){
    run('if ! [ -d '.get('deploy_path').'/shared/all_migrations'.' ]; then mkdir '.get('deploy_path').'/shared/all_migrations ; fi');
    run('cp -a '.get('deploy_path').'/current/app/db/migrations/. '.get('deploy_path').'/shared/all_migrations/');
});


task('update_modules', function (){
    $releases = get('releases_list');
    $all_path = get('deploy_path').'/shared/all_migrations';
    $curr_path = get('deploy_path').'/releases/'.$releases[1];
    run('/usr/bin/php ' . get('deploy_path') . '/current/console/servant/servant update_modules '.$releases[1]);
});

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'servant_migrate',
    'update_modules',
    'cleanup',
    'success'
]);


task('servant_rollback', function (){
    $releases = get('releases_list');
    $all_path = get('deploy_path').'/shared/all_migrations';
    $curr_path = get('deploy_path').'/releases/'.$releases[1];

    run('/usr/bin/php ' . get('deploy_path') . '/current/console/servant/servant rollback '.$all_path.' '.$curr_path);
});

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

after('deploy', 'copy_migrations');
after('deploy', 'app_version_iterate');

before('rollback', 'servant_rollback');
