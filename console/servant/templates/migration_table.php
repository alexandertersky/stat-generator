<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class ${migration_name}_${time} {
    public function up() {
        Capsule::schema()->create('${table_name}', function($table) {
            ${cols}
        });

        ${insert}
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
