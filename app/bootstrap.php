<?php
use Tracy\Debugger;


session_start();

require __DIR__ . '/../vendor/autoload.php';

// Create app
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

// Get container
$container = $app->getContainer();


//Tracy Debugger
Debugger::enable(false);

require __DIR__.'/routes/api.php';