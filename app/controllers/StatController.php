<?php /** @noinspection SlowArrayOperationsInLoopInspection */

namespace App\Controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class StatController
{
    const COUNT = 5122;
    const NICKS_PATH = __DIR__ . '/../../data/list.json';

    public function generate()
    {
        $stat['date'] = $this->getDates();

        $nicks = json_decode(file_get_contents(self::NICKS_PATH), true);

        $stat['nick'] = array_splice($nicks, 0, self::COUNT);

        $stat['time'] = $this->getData(377, 1, 5000, 0.7, self::COUNT);
        $avg['time'] = array_sum($stat['time']) / self::COUNT;

        $stat['sessions_count'] = $this->getData(1.4, 1, 5, 0.3, self::COUNT);
        $avg['sessions_count'] = array_sum($stat['sessions_count']) / self::COUNT;

        $stat['success'] = $this->getOldData(0.77, 0, 1, 0, self::COUNT);
        $avg['success'] = array_sum($stat['success']) / self::COUNT;


        $data = [];

        for ($i = 0; $i < self::COUNT; $i++) {
            $data[$i] = [
                'nick' => mb_convert_encoding($stat['nick'][$i], 'UTF-8'),
                'time' => $stat['time'][$i],
                'sessions_count' => round($stat['sessions_count'][$i]),
                'success' => round($stat['success'][$i]),
                'date' => $stat['date'][$i]
            ];
        }

        shuffle($data);

        $this->generateExcel($data);
    }

    public function getDates()
    {
        // Февраль - 375 игроков
        // Март - 879 игроков
        // Апрель - 2284 игрока
        // Май - 1530 игроков
        // Июнь - 54 игрока
        $february = $this->getMonthDates(2, 1, 28, 375);
        $march = $this->getMonthDates(3, 1, 31, 879);
        $april = $this->getMonthDates(4, 1, 30, 2284);
        $may = $this->getMonthDates(5, 1, 31, 1530);
        $june = $this->getMonthDates(6, 1, 30, 54);
        return array_merge($february, $march, $april, $may, $june);
    }

    public function getMonthDates($month, $min, $max, $count)
    {
        $days = $this->getOldData(15, $min, $max, 1, $count);
        $days = $this->checkDates($days, $min, $max);

        $holydays_count = 0;
        $max_holydays_count = $count * 0.1;

        $dates = [];

        if (mb_strlen($month) < 2) {
            $month = '0' . $month;
        }

        foreach ($days as &$day) {
            $day = $this->getFormatedDate('2019', $month, $day, $holydays_count, $max_holydays_count);
        }

        return $days;
    }

    public function getFormatedDate($year, $month, $day_num, &$holydays_count, $max_holydays_count, $change_day = false)
    {
        if ($change_day) {
            $day_num = rand(1, 27);
        }
        $day = mb_strlen($day_num) < 2 ? '0' . $day_num : $day_num;

        $is_holyday = in_array([$month, $day], [
            ['02', '02'],
            ['02', '03'],
            ['02', '09'],
            ['02', '10'],
            ['02', '16'],
            ['02', '17'],
            ['02', '23'],
            ['02', '24'],
            ['03', '02'],
            ['03', '03'],
            ['03', '08'],
            ['03', '09'],
            ['03', '10'],
            ['03', '16'],
            ['03', '17'],
            ['03', '23'],
            ['03', '24'],
            ['03', '30'],
            ['03', '31'],
            ['04', '06'],
            ['04', '07'],
            ['04', '13'],
            ['04', '14'],
            ['04', '20'],
            ['04', '21'],
            ['04', '27'],
            ['04', '28'],
            ['05', '01'],
            ['05', '02'],
            ['05', '03'],
            ['05', '04'],
            ['05', '05'],
            ['05', '09'],
            ['05', '10'],
            ['05', '11'],
            ['05', '12'],
            ['05', '18'],
            ['05', '19'],
            ['05', '25'],
            ['05', '26'],
            ['06', '01'],
            ['06', '02'],
            ['06', '08'],
            ['06', '09'],
            ['06', '12'],
            ['06', '15'],
            ['06', '16'],
            ['06', '22'],
            ['06', '23'],
            ['06', '29'],
            ['06', '30']
        ]);

        if ($is_holyday) {
            $holydays_count++;
            if ($max_holydays_count <= $holydays_count) {
                return $this->getFormatedDate($year, $month, $day_num, $holydays_count, $max_holydays_count, true);
            }
        }
        return $day = '2019-' . $month . '-' . $day;
    }

    public function checkDates($array, $min, $max)
    {
        foreach ($array as &$value) {
            if ($value < $min || $value > $max) {
                $value = rand($min, $max);
            }
        }
        return $array;
    }

    public function generateExcel($data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $schema = ['nick' => 'A', 'time' => 'B', 'sessions_count' => 'C', 'success' => 'D', 'date' => 'E'];
        $nick_max_length = 0;
        $row = 1;

        $sheet->setCellValue($schema['nick'] . $row, 'Никнейм');
        $sheet->setCellValue($schema['time'] . $row, 'Время в игре');
        $sheet->setCellValue($schema['sessions_count'] . $row, 'Кол-во игр');
        $sheet->setCellValue($schema['success'] . $row, 'Успешность');
        $sheet->setCellValue($schema['date'] . $row, 'Дата регистрации');
        $row++;

        foreach ($data as $player) {
            $sheet->setCellValue($schema['nick'] . $row, mb_convert_encoding($player['nick'], 'UTF-8'));
            $sheet->getStyle($schema['nick'] . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

            $sheet->setCellValue($schema['time'] . $row, $player['time']);
            $sheet->getStyle($schema['time'] . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

            $sheet->setCellValue($schema['sessions_count'] . $row, $player['sessions_count']);
            $sheet->getStyle($schema['sessions_count'] . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

            $sheet->setCellValue($schema['success'] . $row, $player['success']);
            $sheet->getStyle($schema['success'] . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

            $sheet->setCellValue($schema['date'] . $row, $player['date']);
            $sheet->getStyle($schema['date'] . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

            if (strlen($player['nick']) > $nick_max_length) {
                $nick_max_length = strlen($player['nick']);
            }

            $row++;
        }

        $sheet->getColumnDimension($schema['nick'])->setWidth($nick_max_length * 0.8);
        $sheet->getColumnDimension($schema['time'])->setWidth(12);
        $sheet->getColumnDimension($schema['sessions_count'])->setWidth(11);
        $sheet->getColumnDimension($schema['success'])->setWidth(11);
        $sheet->getColumnDimension($schema['success'])->setWidth(12);

        $writer = new Xlsx($spreadsheet);
        $this->sendFile($writer);
    }

    public function sendFile($writer)
    {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        // если этого не сделать файл будет читаться в память полностью!
        if (ob_get_level()) {
            ob_end_clean();
        }
        // заставляем браузер показать окно сохранения файла
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . 'statistic.xlsx');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
//        header('Content-Length: ' . filesize($file));
        // читаем файл и отправляем его пользователю
        $writer->save('php://output');
        exit;
    }

    public function getOldDataRand($average, $pie, $min, $max)
    {
        $pay = rand($average * (1 - $pie), $average * (1 + $pie));
        if ($pay < $min || $pay > $max) {
            return $this->getOldDataRand($average, $pie, $min, $max);
        }
        return $pay;
    }

    public function getOldData($avg, $min_val, $max_val, $pie, $count)
    {
        $arr = [];
        $summ = $avg * $count;

        for ($i = 0; $i < $count; $i++) {
            $average = $summ / ($count - $i);
            $pay = $this->getOldDataRand($average, $pie, $min_val, $max_val);
            $summ -= $pay;
            $arr[$i] = $pay;
            if ($i + 1 == $count) {
                if ($summ !== 0) {
                    $key = rand(0, $count - 1);
                    $arr[$key] = $arr[$key] + $summ;
                }
            }
        }
        shuffle($arr);
        return $arr;
    }

    public function getData($avg, $min_val, $max_val, $pie, $count)
    {
        $arr = [];
        $summ = $avg * self::COUNT;

        for ($i = 0; $i < self::COUNT; $i++) {
            $average = $summ / ($count - $i);
            $min = $average * (1 - $pie);
            $min = $min > $min_val ? $min : $min_val;
            $max = $average * (1 + $pie);
            $max = $max < $max_val ? $max : $max_val;
            $part = rand($min, $max);
            $summ -= $part;
            $arr[$i] = $part;
            if ($i + 1 == $count) {
                if ($summ !== 0) {
                    $this->normalize($arr, $summ, $min_val, $max_val);
                }
            }
        }

        if ($this->check($arr, $avg)) {
            shuffle($arr);
            return $arr;
        }
        return $this->getData($avg, $min_val, $max_val, $pie, $count);
    }

    public function normalize(&$arr, $summ, $min_val, $max_val)
    {
        $key = rand(0, self::COUNT - 1);
        if ($arr[$key] + $summ <= $min_val || $arr[$key] + $summ >= $max_val) {
            $this->normalize($arr, $summ, $min_val, $max_val);
        } else {
            $arr[$key] = $arr[$key] + $summ;
        }
    }

    public function check($arr, $avg)
    {
        return array_sum($arr) / self::COUNT === $avg;
    }
}